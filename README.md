# Guess the password game

This is a simple game I wrote in about half an hour that lets one person generate a link for many people to try to guess a word. Go to https://danielrparks.gitlab.io/guesspass to play.
