// made in an hour and a half, please don't hate
var guessing = false;
var password = "";
function setup() {
	if (window.location.hash) {
		document.querySelector("#instructions").innerHTML = "Guess the password to win!";
		let box = document.querySelector("#box");
		box.type = "password";
		box.style.display = "";
		guessing = true;
		password = window.location.hash.substr(1);
	}
	else {
		document.querySelector("#instructions").innerHTML = "Enter a password to generate a link for others.";
		let box = document.querySelector("#box");
		box.type = "text";
		box.style.display = "";
	}
	document.querySelector("#box").addEventListener("keydown", submitIfEnter);
	document.querySelector("#submit").addEventListener("click", submit);
}
window.addEventListener("load", setup);

function submitIfEnter(evt) {
	if (evt.key == "Enter" || evt.key == "Return") {
		submit();
	}
}

function submit() {
	if (guessing) {
		let morebox = document.querySelector("#more");
		let guess = document.querySelector("#box").value;
		let guess2 = null;
		if ("#" + md5(guess) == window.location.hash) {
			guess2 = document.createElement("h1");
			guess2.style.color = "green";
			guess2.innerText = guess;
			document.querySelector("#box").disabled = true;
			document.querySelector("#box").type = "text";
			document.querySelector("#instructions").innerHTML = "<h1 style=\"color: green;\">You win!</h1>";
		}
		else {
			guess2 = document.createElement("span");
			guess2.style.color = "red";
			guess2.style.textDecoration = "line-through";
			guess2.innerText = guess;
			document.querySelector("#box").value = "";
		}
		if (morebox.hasChildNodes()) {
			let inserted = morebox.insertBefore(guess2, morebox.firstChild);
			morebox.insertBefore(document.createElement("br"), inserted.nextSibling);
		}
		else {
			morebox.appendChild(guess2);
			morebox.appendChild(document.createElement("br"));
		}
	}
	else {
		let newurl = window.location.href + "#" + md5(document.querySelector("#box").value);
		document.querySelector("#box").value = "";
		document.querySelector("#box").placeholder = "(password hidden)";
		let morebox = document.querySelector("#more");
		let text1 = document.createTextNode("Send people to ");
		let text2 = document.createTextNode(" to guess the password.");
		let a1 = document.createElement("a");
		a1.innerText = newurl;
		a1.href = newurl;
		morebox.appendChild(text1);
		morebox.appendChild(a1);
		morebox.appendChild(text2);
		let qrbox = document.createElement("div");
		qrbox.id = "qrbox";
		new QRCode(qrbox, newurl);
		morebox.appendChild(qrbox);
	}
}
